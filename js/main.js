$('a[href^="#"]').click(function(){
    var target = $(this).attr('href');
    $('html, body').animate({scrollTop: $(target).offset().top - 250}, 600);
    return false;
});

function dater(d, like_eu) {
    let now = new Date();
    let ma = [];
    now.setDate(now.getDate() + d + 1);

    let dayNum = '';
    if (now.getDate() < 10) {
        dayNum = '0';
    }
    dayNum += now.getDate();

    let monthNum = '';

    monthNum += now.getMonth();

    ma = ['Января', 'Февраля', 'Марта', 'Апреля', 'Мая', 'Июня', 'Июля', 'Августа', 'Сентября', 'Октября', 'Ноября', 'Декабря']

    if (like_eu === true) {
        return dayNum + " " + ma[monthNum] + " " + now.getFullYear();
    } else {
        return dayNum + "." + monthNum + "." + now.getFullYear();
    }
}

let date = dater(-2, false)

$('.date-value').text(date)

let timerBox = document.querySelectorAll('.timer'),
    hoursBox = document.querySelectorAll('.hours'),
    minsBox = document.querySelectorAll('.minutes'),
    secBox = document.querySelectorAll('.sec');


const startTimer = () => {
    let day = new Date(),
        hours = day.getHours(),
        mins = day.getMinutes(),
        sec = day.getSeconds(),
        diffH, diffM, diffS;

    diffH = 23 - hours;
    diffM = 59 - mins;
    diffS = 59 - sec;

    for(let i = 0; i < timerBox.length; i++) {
        hoursBox[i].innerText = diffH < 10 ? '0' + diffH : diffH;
        minsBox[i].innerText = diffM < 10 ? '0' + diffM : diffM;
        secBox[i].innerText = diffS < 10 ? '0' + diffS : diffS;
    }
}

setInterval(startTimer, 1000)
startTimer()